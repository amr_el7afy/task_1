import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      title: "Task 1",
      home: home(),
      debugShowCheckedModeBanner: false,
    ));

class home extends StatefulWidget {
  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.teal.shade700,
        appBar: AppBar(
          leading: Icon(Icons.arrow_back),
          actions: <Widget>[
            Icon(
              Icons.more_vert,
              size: 30,
            ),
            Icon(
              Icons.delete,
              size: 30,
            )
          ],
          backgroundColor: Colors.teal.shade700,
          centerTitle: true,
          title: Text(
            'Hello',
            style: TextStyle(fontSize: 23.2),
          ),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Center(
                      child: Icon(
                        Icons.home,
                        size: 50,
                        color: Colors.grey[600],
                      ),
                    ),
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.deepPurple[900]),
                  ),
                  Container(
                    child: Center(
                      child: Icon(
                        Icons.account_circle,
                        size: 50,
                        color: Colors.grey[600],
                      ),
                    ),
                    margin: EdgeInsets.only(right: 10),
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.deepPurple[900]),
                  ),
                  Container(
                    child: Center(
                      child: Icon(
                        Icons.alternate_email,
                        size: 50,
                        color: Colors.grey[600],
                      ),
                    ),
                    margin: EdgeInsets.only(right: 10),
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.deepPurple[900]),
                  ),
                  Container(
                    child: Center(
                      child: Icon(
                        Icons.apps,
                        size: 50,
                        color: Colors.grey[600],
                      ),
                    ),
                    margin: EdgeInsets.only(right: 10),
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.deepPurple[900]),
                  ),
                  Container(
                    child: Center(
                      child: Icon(
                        Icons.settings,
                        size: 50,
                        color: Colors.grey[600],
                      ),
                    ),
                    margin: EdgeInsets.only(right: 10),
                    width: 70,
                    height: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.deepPurple[900]),
                  )
                ],
              ),
              Container(
                child: Center(
                  child: Text(
                    'welcome...',
                    style: TextStyle(
                        color: Colors.white70,
                        fontWeight: FontWeight.w700,
                        fontSize: 25),
                  ),
                ),
                margin: EdgeInsets.only(bottom: 10),
                width: 400,
                height: 70,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.deepPurple[900]),
              ),
              Container(
                margin: EdgeInsets.only(left: 125, bottom: 10),
                child: Center(
                  child: Text(
                    'welcome',
                    style: TextStyle(
                        color: Colors.white70,
                        fontWeight: FontWeight.w700,
                        fontSize: 19),
                  ),
                ),
                width: 150,
                height: 70,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.deepPurple[900]),
              ),
              Container(
                margin: EdgeInsets.only(left: 125, bottom: 10),
                child: Center(
                  child: Text(
                    'welcome',
                    style: TextStyle(
                        color: Colors.white70,
                        fontWeight: FontWeight.w700,
                        fontSize: 19),
                  ),
                ),
                width: 150,
                height: 70,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.deepPurple[900]),
              ),
              Container(
                margin: EdgeInsets.only(left: 125, bottom: 10),
                child: Center(
                  child: Text(
                    'welcome',
                    style: TextStyle(
                        color: Colors.white70,
                        fontWeight: FontWeight.w700,
                        fontSize: 19),
                  ),
                ),
                width: 150,
                height: 70,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.deepPurple[900]),
              ),
              Container(
                margin: EdgeInsets.only(left: 125, bottom: 10),
                child: Center(
                  child: Text(
                    'welcome',
                    style: TextStyle(
                        color: Colors.white70,
                        fontWeight: FontWeight.w700,
                        fontSize: 19),
                  ),
                ),
                width: 150,
                height: 70,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.deepPurple[900]),
              ),
            ],
          ),
        ));
  }
}
